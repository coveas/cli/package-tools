# Cove Package Tools

Package tools is a collection of commands an utilities for working with packages

## Installation

```
# Clone the repo
git clone git@gitlab.com:coveas/cli/package-tools.git
cd package-tools
composer install

# Copy and configure the .env file 
cp .env.example .env
```


## Usage
```
# 1. Bump command. Bumps a package version
php artisan bump
```
